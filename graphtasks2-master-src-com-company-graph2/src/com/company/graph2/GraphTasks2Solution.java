package com.company.graph2;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;


public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer,Integer> mapOfLength = new HashMap<>();
        ArrayDeque<Integer> queOfVertex = new ArrayDeque<>();
        ArrayList<Integer> usedVertices = new ArrayList<>();
        int maximumLabel = 999999999;
        int minLength = 0;
        int vertex = 0;

        //заполение map максимальными метками, кроме начальной вершины
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if(adjacencyMatrix[0][i] == 0){
                mapOfLength.put(startIndex,0);
                mapOfLength.put(i,maximumLabel);
            }else{
                mapOfLength.put(i,maximumLabel);
            }
        }

        // добавляем первую вершину в очередь
        queOfVertex.addLast(startIndex);

        while (!queOfVertex.isEmpty()) {

            //перезапишем метки у потомков начальной вершины, если расстояние до них меньше максимальной метки
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[queOfVertex.getFirst()][i] + minLength < mapOfLength.get(i)
                        && adjacencyMatrix[queOfVertex.getFirst()][i] != 0) {
                    mapOfLength.put(i, adjacencyMatrix[queOfVertex.getFirst()][i] + minLength);
                }
            }

            int min = 999999999;
            //ищем минимальное расстояние от начальной вершины до ее потомка
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[queOfVertex.getFirst()][i] != 0
                        && adjacencyMatrix[queOfVertex.getFirst()][i] < min && !usedVertices.contains(i)) {
                    min = adjacencyMatrix[queOfVertex.getFirst()][i];
                    vertex = i;
                }
            }

            if (adjacencyMatrix[queOfVertex.getFirst()][vertex]+minLength < mapOfLength.get(vertex)){
                minLength = adjacencyMatrix[queOfVertex.getFirst()][vertex]+minLength;
                mapOfLength.put(vertex,minLength);
            }else{
                minLength = mapOfLength.get(vertex);
            }

            //добавляем вершину с минимальной меткой в очередь
            if (queOfVertex.getFirst() != vertex) {
                queOfVertex.addLast(vertex);
            }

            //удаляем использованную вершину из очереди и добавляем её в список использованных вершин
            usedVertices.add(queOfVertex.removeFirst());
        }

        return mapOfLength;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        ArrayList<Integer> usedVertices = new ArrayList<>();
        ArrayDeque<Integer> queOfVertex = new ArrayDeque<>();
        int minLength = 0;
        int vertex = 0;
        int startIndex = 0;

        usedVertices.add(startIndex);

        while (usedVertices.size()!= adjacencyMatrix.length){

            int min = 999999999;
            //поиск минимального ребра от выбранной вершины

            for (int i = 0; i < usedVertices.size(); i++) {
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if (adjacencyMatrix[usedVertices.get(i)][j] < min && adjacencyMatrix[usedVertices.get(i)][j]!=0 &&
                            !usedVertices.contains(j)){
                        min = adjacencyMatrix[usedVertices.get(i)][j];
                        vertex = j;
                    }
                }
            }

            minLength = minLength + min;

            usedVertices.add(vertex);
        }
        return minLength;

    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        return null;
    }
}
